/* TODO: doxygen comments
 * main.c
 *
 *  Created on: Dec 8, 2012
 *      Author: pf
 */

#include <stdio.h>
#include "conversion.h"

#define SEPARATOR '-'


int main(void){
	uint16_t	TestValue;		/* Value to be converted */
	uint8_t		Result[4];		/* Array to store the result */


	int i;
	/* Initialize array values */
	for(i=0;i<sizeof(Result);i++){
		Result[i]=0;
	}

	/* Get the user's value */
	printf("Enter an integer value: ");
	scanf("%d",&TestValue);

	printf("Value entered: %d\n",TestValue);

	/* Convert and store in Result[] */
	bin2bcd(TestValue,Result);

	/* Print the result */
	printf("Value after conversion to BCD: ");

	putchar(SEPARATOR);
	for(i=sizeof(Result)-1;i>=0;i--){
		putchar('0'+Result[i]);
		putchar(SEPARATOR);
	}
	putchar('\n');

	return 0;
}
