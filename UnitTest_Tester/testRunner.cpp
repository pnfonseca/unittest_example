/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */


#include <CppUTest/TestHarness.h>

extern "C" {
#include "conversion.h"
}

TEST_GROUP(BCDconv)
{
	void setup()
	{

	}

	void teardown()
	{

	}
};


// Return value from legal argument
TEST(BCDconv,ReturnValFromLegalArg)
{

	uint16_t TestValue = 0;
	uint8_t Result[4]={0,0,0,0};
	int8_t testResult;

	testResult = bin2bcd(TestValue, Result);

	CHECK_EQUAL(0,testResult);

}

// Test zero value
TEST(BCDconv,ConvertZeroValue)
{

	uint16_t TestValue = 0;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {0,0,0,0};

	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}

// Test value one
TEST(BCDconv,ConvertValueOne)
{

	uint16_t TestValue = 1;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {1,0,0,0};

	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}


// Test non-zero value, less than 10
TEST(BCDconv,ConvertNonZeroValue1)
{
	uint16_t TestValue = 9;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {9,0,0,0};


	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}

// Test non-zero value, less than 100
TEST(BCDconv,ConvertNonZeroValue2)
{
	uint16_t TestValue = 49;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {9,4,0,0};


	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}

// Test non-zero value, less than 1000
TEST(BCDconv,ConvertNonZeroValue3)
{
	uint16_t TestValue = 698;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {8,9,6,0};


	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}

// Test largest possible value
TEST(BCDconv,ConvertLargestPossibleValue)
{
	uint16_t TestValue = 9999;
	uint8_t Result[4]={0,0,0,0};

	uint8_t Expected[4] = {9,9,9,9};


	int i;

	bin2bcd(TestValue, Result);

	 for(i=0;i<4;i++){
		CHECK_EQUAL(Expected[i],Result[i]);
	}

}



#if 0
// Return value from illegal argument
TEST(BCDconv,ReturnValFromIlegalArg)
{
	uint16_t TestValue = 10000;
	uint8_t Result[4]={0,0,0,0};
	int8_t testResult;

	testResult = bin2bcd(TestValue, Result);

	CHECK_EQUAL(-1,testResult);

}
#endif



